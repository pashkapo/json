<?php

namespace App\Http\Controllers;

use App\Repositories\UsersRepositoryInterface;

class ApiController extends Controller
{
    private $usersRepository;

    public function __construct(UsersRepositoryInterface $usersRepository) {
        $this->usersRepository = $usersRepository;
    }

    public function users() {
        $users = $this->usersRepository->getUsersWithCitiesAndInvitors();
        return response()->json($users);
    }

    public function users_debug() {
        $users = $this->usersRepository->getUsersWithCitiesAndInvitors();
        return response(strval($users));
    }
}
