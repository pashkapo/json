<?php
namespace App\Repositories;

use App\User;
use Illuminate\Support\Facades\DB;

class UsersRepository extends Repository implements UsersRepositoryInterface
{
    /**
     * UsersRepository constructor.
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function getUsersWithCitiesAndInvitors()
    {
        return DB::table('users as u1')
            ->join('cities as c', 'u1.city_id', '=', 'c.id')
            ->leftJoin('users as u2', 'u1.invited_by', '=', 'u2.id')
            ->select('u1.id', 'u1.name', 'c.name as city', 'u2.name as invitor')
            ->get();
    }
}