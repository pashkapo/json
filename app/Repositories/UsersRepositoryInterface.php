<?php

namespace App\Repositories;

interface UsersRepositoryInterface extends RepositoryInterface
{
    public function getUsersWithCitiesAndInvitors();
}