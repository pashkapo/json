# Одно из заданий по курсу "Основы оптимизации веб-сайтов"
Нужно сделать сервис выдающий полный список пользователей в виде JSON.
Нужно использовать ORM или паттерн "Репозиторий".
Нужно запрофилировать формирование JSON.
Формирование JSON должно порождать менее 20 запросов к БД.
```json
[
 {
  "id": 41,
  "name": "Waino Hyatt",
  "city": "Астана",
  "invitor": "Blanche Schowalter"
 }, ...
]
```
# Демо: JSON-ресурсы

Список пользователей, выдаваемый через ресурсы Laravel

## Установка зависимостей
```
 composer install
```

## Инициализация БД

```
 touch storage/app/db.sqlite 
 composer migrate
 composer db:seed
```

## Запуск в режиме сервера

```
 php artisan serve
```

## URL-ы для исследования:

 - JSON: http://127.0.0.1:8000/api/users
 - JSON+debugbar: http://127.0.0.1:8000/api/users_debug

